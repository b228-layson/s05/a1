/*
1. Customer class:

email property - string

cart property - instance of Cart class

orders property - array of objects with stucture {products: cart contents, totalAmount: cart total}

checkOut() method - pushes the Cart instance to the orders array if Cart contents is not empty


2. Product class:

name property - string

price property - number

isActive property - Boolean: defaults to true

archive() method - will set isActive to false if it is true to begin with

updatePrice() method - replaces product price with passed in numerical value


3. Cart class:

contents property - array of objects with structure: {product: instance of Product class, quantity: number}

totalAmount property - number

addToCart() method - accepts a Product instance and a quantity number as arguments, pushes an object with structure: {product: instance of Product class, quantity: number} to the contents property

showCartContents() method - logs the contents property in the console

updateProductQuantity() method - takes in a string used to find a product in the cart by name, and a new quantity. Replaces the quantity of the found product in the cart with the new value.

clearCartContents() method - empties the cart contents

computeTotal() method - iterates over every product in the cart, multiplying product price with their respective quantities. Sums up all results and sets value as totalAmount.
*/