class Customer{
    constructor(email){
        this.email = email;
        this.cart = new Cart;
        this.orders = [];
    }
    checkOut(){
        let cartLength = this.cart.contents.length;
        if (cartLength) {
            this.cart.computeTotal();
            this.orders.push(
            {
                "products": this.cart.contents,
                "totalAmount": this.cart.totalAmount
            }
            );
        } else {
            
            console.log(`${this.cart.contents} cart is empty`);
        }
        return this;
    }
   
}

/* 

1. Customer class:

email property - string

cart property - instance of Cart class

orders property - array of objects with stucture {products: cart contents, totalAmount: cart total}

checkOut() method - pushes the Cart instance to the orders array if Cart contents is not empty


*/



class Product{
    constructor(name, price, isActive){
        this.name = name;
        this.price = price;
        this.isActive = true;
    }
    archive(){
        this.isActive = false;
        return this;
    }
    updatePrice(price){
        this.price = price;
        return this;
    }
}



class Cart{
    constructor(contents, totalAmount){
        this.contents = [];
        this.totalAmount = 0;
    }
     addToCart(product, quantity){
        this.contents.push({product, quantity});
        return this;
    }
    showCartContents(){
        this.contents;
        return this;

    }
    updateProductQuantity(product, quantity){
        this.contents.find(e => e.product.name = product).quantity = quantity;
        return this;

    }
    clearCartContents(){
        this.contents = [];
        return this;
    }
    computeTotal(){
        for (let i = 0; i < this.contents.length; i++) {
        this.totalAmount += this.contents[i].quantity * this.contents[i].product.price;;
        }
        return this; 
    }

}



/* 


3. Cart class:

contents property - array of objects with structure: {product: instance of Product class, quantity: number}

totalAmount property - number

addToCart() method - accepts a Product instance and a quantity number as arguments, pushes an object with structure: {product: instance of Product class, quantity: number} to the contents property

showCartContents() method - logs the contents property in the console

updateProductQuantity() method - takes in a string used to find a product in the cart by name, and a new quantity. Replaces the quantity of the found product in the cart with the new value.

clearCartContents() method - empties the cart contents

computeTotal() method - iterates over every product in the cart, multiplying product price with their respective quantities. Sums up all results and sets value as totalAmount.

*/


const john = new Customer('john@mail.com');

console.log(john);

const prodA = new Product('soap', 9.99);

// const prodB = new Product('shampoo', 10.99);

// const prodC = new Product('toothphaste', 20.99);

//console.log(prodA);

//console.log(prodA.updatePrice(12.99));

//console.log(prodA.archive());





//console.log(john.cart.addToCart(prodA, 1));
// console.log(john.cart.addToCart(prodB, 1));
// console.log(john.cart.addToCart(prodC, 50));



//console.log(john.cart.showCartContents());

//console.log(john.cart.updateProductQuantity('soap', 1));

//console.log(john.cart.computeTotal());

//console.log(john.cart.clearCartContents());

//console.log(john.checkOut());